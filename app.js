let names = ['ayman', 'cynthia', 'valentin'];

function getNames(){
    let result = '';
    setTimeout(() => {
        names.forEach(e => {
            result += `<li>${e}</li>`;
        });
        document.body.innerHTML = result;
    }, 1000);

}

function createName(name, callback){
    setTimeout(() => {
        names.push(name);
        callback();
    }, 2000);
}

getNames();
createName("toto", getNames);