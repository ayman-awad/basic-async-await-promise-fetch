let names = ['ayman', 'cynthia', 'valentin'];

function getNames(){
    let result = '';
    setTimeout(() => {
        names.forEach(e => {
            result += `<li>${e}</li>`;
        });
        document.body.innerHTML = result;
    }, 1000);
}

function createName(name){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            names.push(name);
            const error = false;
            if(!error){
                resolve();
            }else{
                reject("Erreur de merde");
            }
        }, 2000);
    });
}

async function init(){
    await createName("tata");
    getNames();
}
init();


// createName("toto").then(getNames,error => console.log(error));

//=============================================================
//asunc /await / fetch
// async function fetchUser(){
//     const result = await fetch('https://jsonplaceholder.typicode.com/users')

//     const data = await result.json();

//     console.log(data);
// }
// fetchUser();

